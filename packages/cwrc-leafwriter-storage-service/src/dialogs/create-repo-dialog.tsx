import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControlLabel,
  Stack,
  Switch,
  TextField,
} from '@mui/material';
import { useState, type ChangeEvent } from 'react';
import { useTranslation } from 'react-i18next';
import { useActions } from '../overmind';

interface CreateRepoDialogProps {
  onCancel: () => void;
  onCreate: () => void;
  open: boolean;
}

export const CreateRepoDialog = ({ onCancel, onCreate, open }: CreateRepoDialogProps) => {
  const { createRepo } = useActions().cloud;
  const { openDialog } = useActions().ui;

  const { t } = useTranslation();

  const [name, setName] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [privateRepo, setPrivateRepo] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    const inputValue = event.target.value;
    setName(inputValue);
  };

  const handleDescriptionChange = (event: ChangeEvent<HTMLInputElement>) => {
    const inputValue = event.target.value;
    setDescription(inputValue);
  };

  const handleChangePrivateRepo = (event: ChangeEvent<HTMLInputElement>) => {
    setPrivateRepo(event.target.checked);
  };

  const handleCancel = () => onCancel();

  const handleCreate = async () => {
    if (name === '') return;
    setIsLoading(true);
    const repo = await createRepo({ name, description, isPrivate: privateRepo });

    if (!repo) {
      openDialog({
        props: {
          maxWidth: 'xs',
          preventEscape: true,
          severity: 'error',
          title: `${t('SS.cloud.message.repo_creation_error')}`,
          onClose: () => setIsLoading(false),
        },
      });

      return;
    }

    setIsLoading(false);
    onCreate();
  };

  return (
    <Dialog
      aria-describedby="create-repository"
      aria-labelledby="create-repository-title"
      data-testid="save:create-repo-dialog"
      fullWidth
      maxWidth="sm"
      open={open}
    >
      <DialogTitle id="create-repository-title">
        {t('SS.cloud.createRepo.create_repository')}
      </DialogTitle>
      <DialogContent>
        <Stack spacing={4}>
          <TextField
            autoComplete="off"
            autoFocus
            fullWidth
            id="name"
            label={t('SS.commons.name')}
            onChange={handleNameChange}
            placeholder={`${t('SS.cloud.createRepo.repository_name')}`}
            required
            slotProps={{
              htmlInput: { 'data-testid': 'save:create-repo:name-input' },
            }}
            value={name}
            variant="standard"
          />
          <TextField
            autoComplete="off"
            fullWidth
            helperText={t('SS.cloud.createRepo.description_help')}
            id="description"
            label={t('SS.cloud.createRepo.description')}
            onChange={handleDescriptionChange}
            slotProps={{
              htmlInput: { 'data-testid': 'save:create-repo:description-input' },
            }}
            value={description}
            variant="outlined"
          />
          <FormControlLabel
            control={
              <Switch checked={privateRepo} onChange={handleChangePrivateRepo} size="small" />
            }
            label={t('SS.commons.private')}
          />
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancel}>{t('SS.commons.cancel')}</Button>
        <Button
          data-testid="save:create-repo:create-button"
          disabled={name === ''}
          loading={isLoading}
          onClick={handleCreate}
          variant="contained"
        >
          {t('SS.commons.create')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
