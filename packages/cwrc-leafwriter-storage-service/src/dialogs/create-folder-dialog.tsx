import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from '@mui/material';
import { useState, type ChangeEvent } from 'react';
import { useTranslation } from 'react-i18next';
import { useActions } from '../overmind';

interface CreateRepoDialogProps {
  onCancel: () => void;
  onCreate: () => void;
  open: boolean;
}

export const CreateFolderDialog = ({ onCancel, onCreate, open }: CreateRepoDialogProps) => {
  const { createFolder } = useActions().cloud;
  const { openDialog } = useActions().ui;

  const { t } = useTranslation();

  const [name, setName] = useState<string>('');
  const [isLoading, setIsLoading] = useState(false);

  const handleNameChange = (event: ChangeEvent<HTMLInputElement>) => {
    const inputValue = event.target.value;
    setName(inputValue);
  };

  const handleCancel = () => onCancel();

  const handleCreate = async () => {
    if (name === '') return;
    setIsLoading(true);
    const folder = await createFolder(name);

    if (!folder) {
      openDialog({
        props: {
          maxWidth: 'xs',
          preventEscape: true,
          severity: 'error',
          title: `${t('SS.cloud.message.folder_creation_error')}`,
          onClose: () => setIsLoading(false),
        },
      });
      return;
    }

    setIsLoading(false);
    onCreate();
  };

  return (
    <Dialog
      aria-describedby="create-folder"
      aria-labelledby="create-folder-title"
      data-testid="save:create-folder-dialog"
      fullWidth
      maxWidth="sm"
      open={open}
    >
      <DialogTitle id="create-folder-title">{t('SS.cloud.create_folder')}</DialogTitle>
      <DialogContent>
        <TextField
          autoComplete="off"
          autoFocus
          fullWidth
          id="name"
          label={t('SS.commons.name')}
          onChange={handleNameChange}
          placeholder={`${t('SS.cloud.folder_name')}`}
          required
          slotProps={{
            htmlInput: { 'data-testid': 'save:create-folder:name-input' },
          }}
          value={name}
          variant="standard"
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCancel}>{t('SS.commons.cancel')}</Button>
        <Button
          data-testid="save:create-folder:create-button"
          disabled={name === ''}
          loading={isLoading}
          onClick={handleCreate}
          variant="contained"
        >
          {t('SS.commons.create')}
        </Button>
      </DialogActions>
    </Dialog>
  );
};
