import type { DialogBarProps } from '@src/dialogs';

// eslint-disable-next-line @typescript-eslint/consistent-type-definitions
type State = {
  dialogBar: DialogBarProps[];
};

export const state: State = {
  dialogBar: [],
};
