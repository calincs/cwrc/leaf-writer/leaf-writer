module.exports = {
  root: true,
  extends: ['custom'],
  ignorePatterns: ['README.md', '.eslintrc.js', 'tsup.config.ts'],
};
